# #Build
# FROM node:alpine AS builder
# WORKDIR /opt/web
# COPY package.json ./
# RUN npm install
# COPY . ./
# RUN npm run build

#Server
FROM nginx
COPY ./dist /usr/share/nginx/html